Config {
  font = "-*-Fixed-Bold-R-Normal-*-13-*-*-*-*-*-*-*",
  bgColor = "black",
  fgColor = "grey",
  position = TopW L 100,
  commands = [
    Run Cpu ["-L","3","-H","50","--normal","green","--high","red"] 10,
    Run Network "wlan0" [] 10,
    Run Wireless "wlan0" [] 10,
    Run Memory ["-t","Mem: <usedratio>%"] 10,
    Run Swap [] 10,
    Run Date "%a %b %_d %k:%M" "date" 10,
    Run StdinReader
  ],
  sepChar = "%",
  alignSep = "}{",
  template = "%StdinReader% }{ %wlan0% %wlan0wi% | %cpu% | %memory% * %swap%  | <fc=#ee9a00>%date%</fc>"
}

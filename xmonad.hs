import XMonad
import XMonad.Actions.CycleWS
import XMonad.Actions.NoBorders
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.Script
import XMonad.Layout.Accordion
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Spiral
import XMonad.Prompt
import XMonad.Prompt.AppendFile
import XMonad.Prompt.DirExec
import XMonad.Prompt.Shell
import XMonad.Util.EZConfig
import XMonad.Util.Run(spawnPipe)

import System.IO

myKeys = [
  ((controlMask, xK_space), shellPrompt defaultXPConfig),
  ((mod4Mask, xK_s), dirExecPrompt defaultXPConfig spawn "/home/jrx/bin"),
  ((mod4Mask, xK_Left), prevWS),
  ((mod4Mask, xK_Right), nextWS),
  ((mod4Mask .|. shiftMask, xK_Left), shiftToPrev >> prevWS),
  ((mod4Mask .|. shiftMask, xK_Right), shiftToNext >> nextWS),
  ((mod4Mask, xK_z), sendMessage $ Toggle FULL),
  ((mod4Mask, xK_b), sendMessage ToggleStruts),
  ((mod4Mask, xK_x), spawn "xcalib -invert -alter"),
  ((mod4Mask, xK_g), spawn "gvim"),
  ((mod4Mask, xK_c), spawn "chromium-browser"),
  ((mod4Mask, xK_a), appendFilePrompt defaultXPConfig "/home/jrx/notes")
  ]

myLayoutHook = mkToggle (single FULL) $ Tall 1 (0.03) (1/2) ||| spiral (6/7) ||| Accordion

main = do
  xmproc <- spawnPipe "xmobar /home/jrx/.xmonad/xmobar.hs"

  xmonad $ defaultConfig {
    modMask = mod4Mask,
    terminal = "urxvt",
    manageHook = manageDocks <+> manageHook defaultConfig,
    startupHook = execScriptHook "startup",
    layoutHook = smartBorders $ avoidStruts  $  myLayoutHook,
    logHook = dynamicLogWithPP xmobarPP {
      ppOutput = hPutStrLn xmproc,
      ppTitle = xmobarColor "green" "" . shorten 100,
      ppHiddenNoWindows = id
    }
  } `additionalKeys` myKeys
